module App (
    app,
) where

import qualified Data.Aeson as A
import qualified Data.ByteString.Char8 as BS
import qualified Data.ByteString.Lazy.Char8 as BL
import qualified Data.Map as M
import qualified Data.Set as S
import qualified Data.Text as T
import GHC.Generics
import Network.HTTP.Types
import Network.Wai
import Text.Blaze.Html.Renderer.Utf8 (renderHtml)
import qualified Text.Blaze.Html5 as H

notFoundResp = responseBuilder status404 mempty mempty
badRequestResp = responseLBS status400 mempty

data CovidReq = CovidReq
    { name :: T.Text
    , tel :: Int
    , covid19 :: Bool
    }
    deriving (Generic)

instance A.FromJSON CovidReq

data CovidResp = CovidResp
    { name :: T.Text
    , tel :: Int
    , covid19 :: Bool
    , status :: T.Text
    }
    deriving (Generic)

instance A.ToJSON CovidResp

app :: Application
app req respond = case (requestMethod req, rawPathInfo req) of
    (methodGet, "/covid19.html") ->
        if not hasParams
            then respond $ responseFile status200 mempty "static/covid19.html" Nothing
            else case (M.lookup "name" q, M.lookup "tel" q, M.lookup "covid19" q) of
                (Just name, Just tel, parseCovid19 -> Just covid19) -> do
                    respond . responseLBS status200 mempty . renderHtml $
                        H.html $ do
                            H.head $ H.title "SHABAK covid registration"
                            H.body $ do
                                H.h1 "SHABAK covid registration"
                                H.h2 "Registration successfull"
                                H.ul $ do
                                    H.li $ H.toHtml (BS.unpack name)
                                    H.li $ H.toHtml (BS.unpack tel)
                                    H.li $ H.toHtml (show covid19)
                _ -> respond $ badRequestResp "Bad parameters passed"
                
    (methodPost, "/api/covid19") -> do
        dt <- getRequestBodyChunk req
        case A.eitherDecodeStrict' @CovidReq dt of
            Left err -> respond $ badRequestResp (BL.pack err)
            Right CovidReq{..} ->
                let status = "ok"
                 in respond $ responseLBS status200 mempty (A.encode CovidResp{..})
    _ -> respond notFoundResp
  where
    q = M.mapMaybe id . M.fromList $ queryString req
    hasParams = not $ null (S.fromList ["name", "tel", "covid19"] `S.intersection` (S.fromList . map fst . queryString $ req))
    parseCovid19 = \case
        Just "on" -> Just True
        Just _ -> Nothing
        _ -> Just False
